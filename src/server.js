import express from 'express';
import 'express-async-errors';
import AppError from './errors/AppError';
import routes from './routes';
import './database';

const app = express();

app.use(express.json());
app.use(routes);

app.use((err, request, response, _) => {
  console.log(err);
  if (err instanceof AppError) {
    return response.status(err.statusCode).json({
      status: 'error',
      message: err.message,
    });
  }
  return response.status(500).json({
    status: 'error',
    message: 'Erro interno no sistema.',
  });
});

const port = 3000;
app.listen(port, () => {
  console.log(`Server running at ${port}`);
});
