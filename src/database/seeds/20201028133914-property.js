module.exports = {
  up: async (QueryInterface) => {
    await QueryInterface.bulkInsert(
      'Properties',
      [
        {
          codigo: 1000000001,
          tipo: "sala comercial",
          n_quartos: 1,
          s_publicado: false,
          s_alugado: false,
          imobiliaria_id: 1
        },
        {
          codigo: 1000000002,
          tipo: "casa",
          n_quartos: 2,
          s_publicado: false,
          s_alugado: false,
          imobiliaria_id: 1
        },
        {
          codigo: 1000000003,
          tipo: "galpão",
          n_quartos: 3,
          s_publicado: false,
          s_alugado: false,
          imobiliaria_id: 2
        },
        {
          codigo: 1000000004,
          tipo: "apartamento",
          n_quartos: 4,
          s_publicado: false,
          s_alugado: false,
          imobiliaria_id: 2
        },
        {
          codigo: 1000000005,
          tipo: "sala comercial",
          n_quartos: 5,
          s_publicado: false,
          s_alugado: true,
          imobiliaria_id: 1
        },
      ],
      {}
    );
  },

  down: () => {},
};
