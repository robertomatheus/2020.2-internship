import CreatePropertyService from '../services/PropertyService';

export default class PropertiesController {
  async index(req, res) {
    const {n_quartos} = req.query;
    const createProperty = new CreatePropertyService();
    const properties = await createProperty.index({n_quartos});
    res.status(200).json(properties);
  }

  async create(req, res) {
    const { codigo, tipo, n_quartos, s_publicado, s_alugado, imobiliaria_id } = req.body;
    const createProperty = new CreatePropertyService();
    const property = await createProperty.create({
      codigo,
      tipo,
      n_quartos,
      s_publicado,
      s_alugado,
      imobiliaria_id
    })
    res.status(200).json(property);
  }

  async show(req, res) {
    const {property_id} = req.params;
    const createProperty = new CreatePropertyService();
    const property = await createProperty.get({property_id});
    res.status(200).json(property);
  }

  async update(req, res) {
    const updatedValues = req.body;
    const {property_id} = req.params;
    const createProperty = new CreatePropertyService();
    await createProperty.update({updatedValues, property_id});
    res.status(200).json({"message": "Imóvel atualizado com sucesso"});
  }

  async delete(req, res) {
    const {property_id} = req.params;
    const createProperty = new CreatePropertyService();
    await createProperty.delete({property_id});
    res.status(200).json({ message: 'Imóvel deletado com sucesso!' });
  }
}
