import Sequelize, { Model } from 'sequelize';

class Property extends Model {
  static init(sequelize) {
    super.init(
      {
        codigo: Sequelize.BIGINT,
        tipo: Sequelize.ENUM('apartamento', 'casa', 'galpão', 'sala comercial'),
        n_quartos: Sequelize.INTEGER,
        s_publicado: Sequelize.BOOLEAN,
        s_alugado: Sequelize.BOOLEAN,
      },
      {
        sequelize,
      }
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.RealEstate, { foreignKey: 'imobiliaria_id'});
  }
}

export default Property;
