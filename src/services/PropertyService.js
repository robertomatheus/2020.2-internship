import Sequelize from 'sequelize';
import AppError from '../errors/AppError';
import Property from '../models/Property';
import RealEstate from '../models/RealEstate';

class CreatePropertyService {
  async index({n_quartos}) {
    const properties = await Property.findAll({
      where: {
        s_alugado: false,
        n_quartos: {[Sequelize.Op.gte]: n_quartos?n_quartos:0},
      }
    });
    return properties;
  }

  async create({ codigo, tipo, n_quartos, s_publicado, s_alugado, imobiliaria_id }) {
    const checkRealEstateExists = await RealEstate.findOne({
      where: { id: imobiliaria_id }
    });

    if(!checkRealEstateExists) {
      throw new AppError('Imobiliária não encontrada', 404);
    }

    const checkPropertyExists = await Property.findOne({
      where: {codigo}
    });

    if(checkPropertyExists) {
      throw new AppError('Já existe um imóvel cadastrado com esse código', 400);
    }

    const property = Property.create({
      codigo,
      tipo,
      n_quartos,
      s_publicado,
      s_alugado,
      imobiliaria_id,
    });

    return property;
  }

  async get({property_id}) {
    const property = await Property.findOne({
      where: {id: property_id}
    });

    if(!property) {
      throw new AppError('Imóvel não encontrado', 400);
    }

    return property;
  }

  async update({ updatedValues, property_id }) {
    const checkPropertyExists = await Property.findOne({
      where: {id: property_id}
    });

    if(!checkPropertyExists) {
      throw new AppError('Imóvel não encontrado', 400);
    }

    if(updatedValues.imobiliaria_id && checkPropertyExists.imobiliaria_id != updatedValues.imobiliaria_id) {
      throw new AppError('Não é possível alterar a imobiliária do imóvel', 400);
    }

    await Property.update(
      updatedValues,
      { where:
        { id: property_id }
      }
    ).catch(function() {
      throw new AppError('Não foi possível atualizar o imóvel', 400);
    });
  }

  async delete({property_id}) {
    const property = await Property.findOne({
      where: {id: property_id}
    });

    if (!property) {
      throw new AppError('Imóvel não encontrado', 400);
    }
    else {
      property.destroy();
    }
  }
}

export default CreatePropertyService;
