import CreateRealEstateService from '../services/RealEstateService';

export default class RealEstateController {
  async index(req, res) {
    const createRealEstate = new CreateRealEstateService();
    const realEstate = await createRealEstate.index();
    res.status(200).json(realEstate);  }

  async create(req, res) {
    const { nome, cnpj } = req.body;
    const createRealEstate = new CreateRealEstateService();
    const realEstate = await createRealEstate.create({
      nome,
      cnpj
    })
    res.status(200).json(realEstate);
  }

  async show(req, res) {
    const {realEstate_id} = req.params;
    const createRealEstate = new CreateRealEstateService();
    const realEstate = await createRealEstate.get({realEstate_id});
    res.status(200).json(realEstate);
  }

  async update(req, res) {
    const updatedValues = req.body;
    const {realEstate_id} = req.params;
    const createRealEstate = new CreateRealEstateService();
    await createRealEstate.update({updatedValues, realEstate_id});
    res.status(200).json({"message": "Imobiliária atualizada com sucesso"});
  }

  async delete(req, res) {
    const {realEstate_id} = req.params;
    const createRealEstate = new CreateRealEstateService();
    await createRealEstate.delete({realEstate_id});
    res.status(200).json({ message: 'Imobiliária deletada com sucesso!' });
  }
}
