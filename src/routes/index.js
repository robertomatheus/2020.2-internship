import { Router } from 'express';
import propertiesRouter from './properties.routes';
import realEstateRouter from './realEstate.routes';


const routes = Router();

routes.get('/', (request, response) => {
  response.json({ message: 'Hello from RuaDois!' });
});

routes.use('/imobiliarias', realEstateRouter);
routes.use('/imoveis', propertiesRouter);

export default routes;
