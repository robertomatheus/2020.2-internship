import Sequelize from 'sequelize';

import Property from '../models/Property';
import RealEstate from '../models/RealEstate';

import databaseConfig from '../config/database';

const models = [
  RealEstate,
  Property
];

class Database {
  constructor() {
    this.init();
  }

  init() {
    this.connection = new Sequelize(databaseConfig);

    models
      .map((model) => model.init(this.connection))
      .map(
        (model) => model.associate && model.associate(this.connection.models)
      );
  }
}

export default new Database();
