import { Router } from 'express';
import RealEstateController from '../controllers/RealEstateController';

const realEstateRouter = Router();
const realEstateController = new RealEstateController();

realEstateRouter.get('/', realEstateController.index);
realEstateRouter.post('/', realEstateController.create);
realEstateRouter.get('/:realEstate_id', realEstateController.show);
realEstateRouter.put('/:realEstate_id', realEstateController.update);
realEstateRouter.delete('/:realEstate_id', realEstateController.delete);

export default realEstateRouter;
