import { Router } from 'express';
import PropertiesController from '../controllers/PropertiesController';

const propertiesRouter = Router();
const propertiesController = new PropertiesController();

propertiesRouter.get('/', propertiesController.index);
propertiesRouter.post('/', propertiesController.create);
propertiesRouter.get('/:property_id', propertiesController.show);
propertiesRouter.put('/:property_id', propertiesController.update);
propertiesRouter.delete('/:property_id', propertiesController.delete);

export default propertiesRouter;
