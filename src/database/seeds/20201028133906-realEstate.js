module.exports = {
  up: async (QueryInterface) => {
    await QueryInterface.bulkInsert(
      'RealEstates',
      [
        {
          nome: 'Imobiliária 1',
          cnpj: 10000000001,
        },
        {
          nome: 'Imobiliária 2',
          cnpj: 10000000002,
        },
        {
          nome: 'Imobiliária 3',
          cnpj: 10000000003,
        },
        {
          nome: 'Imobiliária 4',
          cnpj: 10000000004,
        },
        {
          nome: 'Imobiliária 5',
          cnpj: 10000000005,
        },
      ],
      {}
    );
  },

  down: () => {},
};
