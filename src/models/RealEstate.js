import Sequelize, { Model } from 'sequelize';

class RealEstate extends Model {
  static init(sequelize) {
    super.init(
      {
        nome: Sequelize.STRING,
        cnpj: Sequelize.BIGINT
      },
      {
        sequelize,
      }
    );
    return this;
  }
}

export default RealEstate;
