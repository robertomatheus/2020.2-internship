module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Properties', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      codigo: {
        type: Sequelize.BIGINT,
        unique: true,
        allowNull: false
      },
      tipo: {
        type: Sequelize.ENUM('apartamento', 'casa', 'galpão', 'sala comercial'),
        allowNull: false,
      },
      n_quartos: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      s_publicado: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      s_alugado: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      imobiliaria_id: {
        type: Sequelize.INTEGER,
        references: { model: 'RealEstates', key: 'id' },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
        allowNull: false,
      },
      createdAt: {
        type: Sequelize.DATE,
        defaultValue: new Date()
      },
      updatedAt: {
        type: Sequelize.DATE,
        defaultValue: new Date()
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Properties');
  }
};
