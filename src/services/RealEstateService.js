import { where } from 'sequelize';
import AppError from '../errors/AppError';
import RealEstate from '../models/RealEstate';
import RealEstate from '../models/RealEstate';

class CreateRealEstateService {
  async index() {
    const realEstate = await RealEstate.findAll();
    return realEstate;
  }

  async create({ nome, cnpj }) {
    const checkRealEstateExists = await RealEstate.findOne({
      where: {cnpj}
    });

    if(checkRealEstateExists) {
      throw new AppError('Já existe uma imobiliária cadastrada com esse CNPJ', 400);
    }

    const realEstate = RealEstate.create({
      nome,
      cnpj,
    });

    return realEstate;
  }

  async get({realEstate_id}) {
    const realEstate = await RealEstate.findOne({
      where: {id: realEstate_id}
    });

    if(!realEstate) {
      throw new AppError('Imobiliária não encontrada', 400);
    }

    return realEstate;
  }

  async update({ updatedValues, realEstate_id }) {
    const checkRealEstateExists = await RealEstate.findOne({
      where: {id: realEstate_id}
    });

    if(!checkRealEstateExists) {
      throw new AppError('Imobiliária não encontrada', 400);
    }

    await RealEstate.update(
      updatedValues,
      { where:
        { id: realEstate_id }
      }
    ).catch(function() {
      throw new AppError('Não foi possível atualizar a imobiliária', 400);
    });
  }

  async delete({realEstate_id}) {
    const realEstate = await RealEstate.findOne({
      where: {id: realEstate_id}
    });

    if (!realEstate) {
      throw new AppError('Imobiliária não encontrada', 400);
    }
    else {
      realEstate.destroy();
    }
  }
}

export default CreateRealEstateService;
